# README #

* This repo demonstrates how to make GET/POST/DELETE method calls to imgur's api using Retrofit. The UI for this app is fairly simplistic. We start by allowing the user to grab two images from the gallery. Then we create an album using those images. Finally we allow the user to delete the gallery.
* Version: 1.0

### How do I get set up? ###

* Clone the repo locally
* Open the project in Android Studio